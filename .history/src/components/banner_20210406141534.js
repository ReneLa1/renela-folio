import * as React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import { getImage } from "gatsby-plugin-image"
import { convertToBgImage } from "gbimage-bridge"

const Banner = () => {}
  const {bgImage}=useStaticQuery(
    graphql `
    query{
       bgImage:file(relativePath: {eq: "bgImage.png"}) {
    id
    childImageSharp {
      gatsbyImageData(width: 1920, quality: 50, webpOptions: {quality: 70})
    }
  }
    }
    `
  )
  return()
 
  }

export default Banner
