import React from "react"
import { StaticImage } from "gatsby-plugin-image"

const Bio = () => {
  return (
    <div className="container bio">
      <div className="profile-photo">
        <StaticImage width={280} height={280} src="trex.png"  />
        <p className="name">Rene</p>
      </div>
      <div class="bio-description">
        <div className="bio-title">Biography</div>
        <div className="bio-content">bio content</div>
        <div className="personal-info">
          <div className="info-title">Personal Info</div>
          <div className="info-content">
            <div className="row-1">name: Rene La</div>
            <div className="row-2">Phone: 078947329</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Bio
