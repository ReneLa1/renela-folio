import React from "react"

const Bio = () => {
  return (
    <div className="container bio" style={{ backgroundColor: "red" }}>
      <div className="profile-photo">
        <div className="name">Rene</div>
      </div>
      <div class="bio-description">
        <div className="bio-title">Biography</div>
        <div className="bio-content">bio content</div>
        <div className="personal-info">
          <div className="info-title">Personal Info</div>
          <div className="info-content">
            <div className="row-1">name: Rene La</div>
            <div className="row-2">Phone: 078947329</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Bio
