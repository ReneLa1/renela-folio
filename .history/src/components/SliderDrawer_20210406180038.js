import React, { useContext } from "react"
import { MenuContext } from "../utils/MenuContext"
import { Link } from "gatsby"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"


const SliderDrawer = () => {
  const { menuOpen, setMenuOpen } = useContext(MenuContext)
  return (
    <div className={menuOpen ? "side-drawer open" : "side-drawer"}>
      <div className="side-nav-info">
        <p>
          I am available for freelance work. Feel free to <span>discuss</span>
          about your project.
        </p>
      </div>
      <div className="social-icons">
        <Link to="/twitter">
          <FontAwesomeIcon icon={["fab", "facebook-f"]} />
        </Link>
        <Link to="/twitter">
          <FontAwesomeIcon icon={["fab", "twitter"]} />
        </Link>
        <Link to="/instagram">
          <FontAwesomeIcon icon={["fab", "instagram"]} />
        </Link>
        <Link to="/linkedIn">
          <FontAwesomeIcon icon={["fab", "linkedin"]} />
        </Link>
      </div>
    </div>
  )
}

export default SliderDrawer
