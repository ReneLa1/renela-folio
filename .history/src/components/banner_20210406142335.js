import * as React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import { getImage } from "gatsby-plugin-image"
import { convertToBgImage } from "gbimage-bridge"
import BackgroundImage from "gatsby-background-image"

const GbiBridged = () => {
  const { backgroundImage123 } = useStaticQuery(
    graphql`
      query {
        backgroundImage123: file(relativePath: { eq: "bgImage.png" }) {
          childImageSharp {
            gatsbyImageData(
              width: 1920
              quality: 50
              webpOptions: { quality: 70 }
            )
          }
        }
      }
    `
  )

  const image = getImage(backgroundImage123)
  const bgImage = convertToBgImage(image)

  return(
    <BackI></BackI>
  )
}

const Banner = () => {
  return (
    <div>
      <section className="main">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="menu-btn">
                <div className="line-menu half start" />
                <div className="line-menu" />
                <div className="line-menu half end" />
              </div>
              <div className="description">
                <h4>
                  <span>Rene</span>
                  La
                </h4>
                <p className="mb-25">
                  A freelance UI/UX designer and Front-end developer,
                  <br />
                  Based in San Francisco
                </p>
                <Link to="/" className="g-link">
                  Get Resume
                </Link>
              </div>
            </div>
            <div></div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Banner
