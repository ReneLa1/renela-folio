import * as React from "react"
import { Link } from "gatsby"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const Header = ({ location }) => {
  console.log(location)

  const indexHeader = (
    <header>
      <div className="container">
        <div className="inner-header">
          <div className="logo">
            <Link to="/">Rene La</Link>
          </div>
          <div className="navigation">
            <nav>
              <Link to="/twitter">
                <FontAwesomeIcon icon={["fab", "facebook-f"]} />
              </Link>
              <Link to="/twitter">
                <FontAwesomeIcon icon={["fab", "twitter"]} />
              </Link>
              <Link to="/instagram">
                <FontAwesomeIcon icon={["fab", "instagram"]} />
              </Link>
              <Link to="/linkedIn">
                <FontAwesomeIcon icon={["fab", "linkedin"]} />
              </Link>
            </nav>
          </div>
        </div>
      </div>
    </header>
  )

  const routeHeader = (
    <header>
      <div className="container">
        <div className="inner-header">
          <div className="logo">
            <Link to="/">Rene La</Link>
          </div>
          <div className="navigation">
            <nav>
              <Link to="/twitter">
                <FontAwesomeIcon icon={["fab", "facebook-f"]} />
              </Link>
              <Link to="/twitter">
                <FontAwesomeIcon icon={["fab", "twitter"]} />
              </Link>
              <Link to="/instagram">
                <FontAwesomeIcon icon={["fab", "instagram"]} />
              </Link>
              <Link to="/linkedIn">
                <FontAwesomeIcon icon={["fab", "linkedin"]} />
              </Link>
            </nav>
          </div>
        </div>
      </div>
    </header>
  )

  const header = location.pathname==='/'?
  return (
    <header>
      <div className="container">
        <div className="inner-header">
          <div className="logo">
            <Link to="/">Rene La</Link>
          </div>
          <div className="navigation">
            <nav>
              <Link to="/twitter">
                <FontAwesomeIcon icon={["fab", "facebook-f"]} />
              </Link>
              <Link to="/twitter">
                <FontAwesomeIcon icon={["fab", "twitter"]} />
              </Link>
              <Link to="/instagram">
                <FontAwesomeIcon icon={["fab", "instagram"]} />
              </Link>
              <Link to="/linkedIn">
                <FontAwesomeIcon icon={["fab", "linkedin"]} />
              </Link>
            </nav>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
