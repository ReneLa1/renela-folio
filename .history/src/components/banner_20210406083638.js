import * as React from "react"

const Banner = () => (
  <section className="main">
    <div className="container">
      <div className="row">
        <div className="col">
          <div className="menu-btn">
            <div className="line-menu half start" />
            <div className="line-menu" />
            <div className="line-menu half end" />
          </div>
          <div className="description">
            <h4>
              <span>Rene</span>
              La
            </h4>
            <p>
              A freelance UI/UX designer and Front-end developer,
              <br />
              Based in San Francisco
            </p>
          </div>
        </div>
        <div></div>
      </div>
    </div>
  </section>
)

export default Banner
