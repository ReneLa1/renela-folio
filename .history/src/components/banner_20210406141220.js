import * as React from "react"
import { Link, graphql,use } from "gatsby"
import { getImage } from "gatsby-plugin-image"
const Banner = () => (
  <div style={{}}>
    <section className="main">
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="menu-btn">
              <div className="line-menu half start" />
              <div className="line-menu" />
              <div className="line-menu half end" />
            </div>
            <div className="description">
              <h4>
                <span>Rene</span>
                La
              </h4>
              <p className="mb-25">
                A freelance UI/UX designer and Front-end developer,
                <br />
                Based in San Francisco
              </p>
              <Link to="/" className="g-link">
                Get Resume
              </Link>
            </div>
          </div>
          <div></div>
        </div>
      </div>
    </section>
  </div>
)

export default Banner
