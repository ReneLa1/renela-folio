import React, { useContext } from "react"
import { MenuContext } from "../utils/MenuContext"

const SliderDrawer = () => {
  const { menuOpen, setMenuOpen } = useContext(MenuContext)
  return <div className="side-drawer"></div>
}

export default SliderDrawer
