import React, { useContext } from "react"
import { MenuContext } from "../utils/MenuContext"
import { Link } from "gatsby"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const SliderDrawer = () => {
  const { menuOpen, setMenuOpen } = useContext(MenuContext)
  return (
    <div className={menuOpen ? "side-drawer open" : "side-drawer"}>
      <div className="side-nav-info">
        <p>
          I am available for freelance work. Feel free to <span>discuss </span>
          about your project.
        </p>
        <div className="social-icons">
          <Link to="/twitter" style={{ paddingRight: 13 }}>
            <FontAwesomeIcon icon={["fab", "facebook-f"]} />
          </Link>
          <Link to="/twitter" style={{ paddingLeft: 13, paddingRight: 13 }}>
            <FontAwesomeIcon icon={["fab", "twitter"]} />
          </Link>
          <Link to="/instagram" style={{ paddingLeft: 13, paddingRight: 13 }}>
            <FontAwesomeIcon icon={["fab", "instagram"]} />
          </Link>
          <Link to="/linkedIn" style={{ paddingLeft: 13 }}>
            <FontAwesomeIcon icon={["fab", "linkedin"]} />
          </Link>
        </div>
      </div>
      <ul>
        <li className="show">
          <Link href="/" style={{ color: "#dc3545" }}>
            <span>About</span>
          </Link>
        </li>
        <li className="show">
          <Link href="#" style={{ color: "#33d06c" }}>
            <span>Resume</span>
          </Link>
        </li>
        <li className="show">
          <Link href="#" style={{ color: "#ffc107" }}>
            <span>Portfolio</span>
          </Link>
        </li>
        <li className="show">
          <Link href="#" style={{ color: "#17a2b8" }}>
            <span>Blog</span>
          </Link>
        </li>
        <li className="show">
          <Link href="#" style={{ color: "#F5CBA7" }}>
            <span>Contact</span>
          </Link>
        </li>
      </ul>
    </div>
  )
}

export default SliderDrawer
