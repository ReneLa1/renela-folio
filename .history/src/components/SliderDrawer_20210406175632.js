import React, { useContext } from "react"
import { MenuContext } from "../utils/MenuContext"

const SliderDrawer = () => {
  const { menuOpen, setMenuOpen } = useContext(MenuContext)
  return <div className={menuOpen ? "side-drawer open" : "side-drawer"}>
  <div></div>
  </div>
}

export default SliderDrawer
