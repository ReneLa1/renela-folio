import * as React from "react"
import { Link } from "gatsby"

const Banner = () => {
  const 
  return (
    <section className="main">
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="menu-btn">
              <div className="line-menu half start" />
              <div className="line-menu" />
              <div className="line-menu half end" />
            </div>
            <div className="description">
              <h4>
                <span>Rene</span>
                La
              </h4>
              <p className="mb-25">
                A freelance UI/UX designer and Front-end developer,
                <br />
                Based in San Francisco
              </p>
              <Link to="/" className="g-link">
                Get Resume
              </Link>
            </div>
          </div>
          <div className="contact_info">
            <h5 className="mb-25">Let's work together</h5>
            <p className="mb-10">I’m available at</p>
            <p className="mb-1">renela79@gmail.com</p>
            <p className="mb-0">(+250) 780 189 335</p>

            <div className="social-links" />
          </div>
        </div>
      </div>
    </section>
  )
}

export default Banner
