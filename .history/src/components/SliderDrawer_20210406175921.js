import React, { useContext } from "react"
import { MenuContext } from "../utils/MenuContext"

const SliderDrawer = () => {
  const { menuOpen, setMenuOpen } = useContext(MenuContext)
  return (
    <div className={menuOpen ? "side-drawer open" : "side-drawer"}>
      <div className="side-nav-info">
        <p>
          I am available for freelance work. Feel free to <span>discuss</span>
          about your project.
        </p>
      </div>
    </div>
  )
}

export default SliderDrawer
