/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, { useState, useMemo } from "react"
import PropTypes from "prop-types"
import Header from "./header"
import "./layout.css"
import { MenuContext } from "../utils/MenuContext"

import { library } from "@fortawesome/fontawesome-svg-core"
import { fab } from "@fortawesome/free-brands-svg-icons"
import { faCheckSquare, faCoffee } from "@fortawesome/free-solid-svg-icons"

library.add(fab, faCheckSquare, faCoffee)

const Layout = ({ children }) => {
  const [menuOpen, setOpenMenu] = useState(false)
  const providerValue = useMemo(() => ({ menuOpen, setOpenMenu }), [
    menuOpen,
    setOpenMenu,
  ])
  return (
    <MenuContext.Provider value={providerValue}>
    <
      <Header />
      <main>{children}</main>
    </MenuContext.Provider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
