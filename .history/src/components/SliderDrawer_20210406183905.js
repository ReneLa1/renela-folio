import React, { useContext } from "react"
import { MenuContext } from "../utils/MenuContext"
import { Link } from "gatsby"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const SliderDrawer = () => {
  const { menuOpen, setMenuOpen } = useContext(MenuContext)
  return (
    <div className={menuOpen ? "side-drawer open" : "side-drawer"}>
      <div className="side-nav-info">
        <p>
          I am available for freelance work. Feel free to <span>discuss </span>
          about your project.
        </p>
        <div className="social-icons">
          <Link to="/twitter" style={{ paddingRight: 13 }}>
            <FontAwesomeIcon icon={["fab", "facebook-f"]} />
          </Link>
          <Link to="/twitter" style={{ paddingLeft: 13, paddingRight: 13 }}>
            <FontAwesomeIcon icon={["fab", "twitter"]} />
          </Link>
          <Link to="/instagram" style={{ paddingLeft: 13, paddingRight: 13 }}>
            <FontAwesomeIcon icon={["fab", "instagram"]} />
          </Link>
          <Link to="/linkedIn" style={{ paddingLeft: 13 }}>
            <FontAwesomeIcon icon={["fab", "linkedin"]} />
          </Link>
        </div>
      </div>
      <ul>
        <li className="show">
          <a href="#" style={{ color: "#F2C44C" }}>
            <span>About</span>
          </a>
        </li>
        <li className="show">
          <a href="#" style={{ color: "#33d06c" }}>
            <span>Resume</span>
          </a>
        </li>
        <li className="show">
          <a href="#" style={{ color: "#F2C44C" }}>
            <span>Portfolio</span>
          </a>
        </li>
        <li className="show">
          <a href="#" style={{ color: "" }}>
            <span>Blog</span>
          </a>
        </li>
        <li className="show">
          <a href="#" style={{ color: "" }}>
            <span>Contact</span>
          </a>
        </li>
      </ul>
    </div>
  )
}

export default SliderDrawer
