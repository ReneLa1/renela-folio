import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import { Link, graphql, useStaticQuery } from "gatsby"
import { sta } from "gatsby-plugin-image"


import "../styles/styles.scss"
const IndexPage = () => {
  return (
    <Layout>
      <Banner />
    </Layout>
  )
}

export default IndexPage
