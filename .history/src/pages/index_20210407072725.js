import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import bgImg from "../images/bgImage.png"
const IndexPage = ({ location,data }) => {
  const [loading, setLoading] = React.useState(false)
  React.useEffect(() => {
    if (location.href) {
      setLoading(false)
    } else {
      setLoading(true)
    }
  }, [location])
  console.log(location, loading)
  return (
    <Layout>
      <Banner />
    </Layout>
  )
}

export default IndexPage
