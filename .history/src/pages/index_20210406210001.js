import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import { Link, graphql, useStaticQuery } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "../styles/styles.scss"
const IndexPage = () => {
  return (
    <StaticImage></StaticImage>
    <Layout>
      <Banner />
    </Layout>
  )
}

export default IndexPage
