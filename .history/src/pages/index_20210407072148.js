import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import bgImg from "../images/bgImage.png"
const IndexPage = ({ location }) => {
  const backgroundStyle = {
    backgroundImage: `linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), 
    url(${bgImg})`,
    backgroundSize: "auto",
    height: "100vh",
    width: "100%",
    backgroundRepeat: "no-repeat",
  }
  return (
    <Layout>
      <Banner />
    </Layout>
  )
}

export default IndexPage
