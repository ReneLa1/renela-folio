import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import "../styles/styles.scss"
const IndexPage = () => (
  <Layout>
    <Banner />
  </Layout>
)

export default IndexPage
