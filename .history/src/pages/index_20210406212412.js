import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import bgImg from 
const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `url(https://image.tmdb.org/t/p/w500${this.state.background})`,
    backgroundSize: "cover",
    height: "100vh",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 99,
  }
  return (
    <div style={backgroundStyle}>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
