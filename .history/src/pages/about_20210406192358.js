import React from "react"
import Layout from "../components/layout"

const About = () => {
  return <Layout<div>about</div>
}

export default About
