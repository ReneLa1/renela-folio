import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import bgImg from "../images/bgImage.png"
const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), 
    url(${bgImg})`,
    backgroundSize: "contain",
    height: "100vh",
    backgroundRepeat: "no-repeat",
    align
  }
  return (
    <div style={backgroundStyle}>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
