import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import { Link, graphql, useStaticQuery } from "gatsby"
import { getImage } from "gatsby-plugin-image"
import { BgImage } from "gbimage-bridge"
import "../styles/styles.scss"
const IndexPage = () => (
  <Layout>
    <Banner />
  </Layout>
)

export default IndexPage
