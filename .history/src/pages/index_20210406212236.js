import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import { graphql, useStaticQuery } from "gatsby"
import { getImage } from "gatsby-plugin-image"

import { convertToBgImage } from "gbimage-bridge"
import BackgroundImage from "gatsby-background-image"

const IndexPage = () => {

  return (
    <div>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
