import React from "react"
import Layout from "../components/layout"

const About = () => {
  return (
    <Layout>
      <div className="container bio" style={{ backgroundColor: "red" }}>
        <div className="profile-photo">
          <div className="name">Rene</div>
        </div>
        <div class="bio-description">
          <div className="bio-title">Biography</div>
          <div className="bio-content">bio content</div>
        </div>
      </div>
      <div className="container" style={{ backgroundColor: "green" }}>
        What i do
      </div>
      <div className="container" style={{ backgroundColor: "brown" }}>
        Testimonials
      </div>
    </Layout>
  )
}

export default About
