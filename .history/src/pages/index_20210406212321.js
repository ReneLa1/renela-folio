import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"

const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `url(https://image.tmdb.org/t/p/w500${this.state.background})`,
    backgroundSize: "cover",
    height: "100vh",
    position:'absolute'
  }
  return (
    <div>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
