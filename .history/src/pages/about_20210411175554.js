import React from "react"
import Layout from "../components/layout"

const About = () => {
  return (
    <Layout>
      <div className="container" style={{ backgroundColor: "red" }}>
        Biography
      </div>
      <div className="container" style={{ backgroundColor: "green" }}>
        What i do
      </div>
      <div className="container" style={{ backgroundColor: "brown" }}>
        Testimonial
      </div>
    </Layout>
  )
}

export default About
