import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import {} from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "../styles/styles.scss"
const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), 
url()`,

    backgroundSize: "cover",
    height: "100vh",
  }
  return (
    <div>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
