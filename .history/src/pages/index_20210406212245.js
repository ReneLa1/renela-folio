import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"

const IndexPage = () => {
  return (
    <div >
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
