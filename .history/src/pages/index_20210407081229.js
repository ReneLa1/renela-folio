import * as React from "react"
import Banner from "../components/banner"
import Header from "../components/indexHeader"
import Layout from "../components/layout"
import bgImg from "../images/bgImage.png"
const IndexPage = ({ location, data }) => {
  return (
    <>
      <Header />
      <Banner />
    </>
  )
}

export default IndexPage
