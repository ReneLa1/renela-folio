import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import bgImg from "../images/bgImage.png"
const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `linear-gradient(to right, rgba(0, 0, 0, 1), rgba(0, 0, 0, 1)), 
    url(${bgImg})`,
    backgroundSize: "auto",
    height: "100vh",
    width: "100%",
    backgroundRepeat: "no-repeat",
  }
  return (
    <div style={backgroundStyle}>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
