import React from "react"
import Bio from "../components/about/Bio"
import Layout from "../components/layout"

const About = () => {
  return (
    <Layout>
      <Bio />
      <div className="container" style={{ backgroundColor: "green" }}>
        What i do
      </div>
      <div className="container" style={{ backgroundColor: "brown" }}>
        Testimonials
      </div>
    </Layout>
  )
}

export default About
