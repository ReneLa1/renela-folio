import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import { Link, graphql, useStaticQuery } from "gatsby"
import { getImage } from "gatsby-plugin-image"
import { BgImage } from "gbimage-bridge"

import "../styles/styles.scss"
const IndexPage = () => {
  const { backgroundImage123 } = useStaticQuery(
    graphql`
      query {
        backgroundImage123: file(relativePath: { eq: "bgImage.png" }) {
          childImageSharp {
            gatsbyImageData(
              width: 1920
              quality: 50
              webpOptions: { quality: 70 }
            )
          }
        }
      }
    `
  )

  const pluginImage = getImage(backgroundImage123)
  return (
    <BgImage image>
      <Layout>
        <Banner />
      </Layout>
    </BgImage>
  )
}

export default IndexPage
