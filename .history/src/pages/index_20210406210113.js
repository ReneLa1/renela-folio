import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import {} from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "../styles/styles.scss"
const IndexPage = () => {
  return (
    <StaticImage width="100%" src="../images/bgImage.png" height="100vh">
      <Layout>
        <Banner />
      </Layout>
    </StaticImage>
  )
}

export default IndexPage
