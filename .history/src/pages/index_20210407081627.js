import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"
import bgImg from "../images/bgImage.png"
const IndexPage = ({ location }) => {
  const [loading, setLoading] = React.useState(false)
 
  return (
    <Layout>
      <Banner />
    </Layout>
  )
}

export default IndexPage
