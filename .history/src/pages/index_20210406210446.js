import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"

import "../styles/styles.scss"
const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), 
url('../images/bgImage.png)`,

    backgroundSize: "cover",
    height: "100vh",
    width: "100%",
  }
  return (
    <Layout>
      <Banner />
    </Layout>
  )
}

export default IndexPage
