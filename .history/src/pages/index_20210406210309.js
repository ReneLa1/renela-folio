import * as React from "react"
import Banner from "../components/banner"
import Layout from "../components/layout"

import "../styles/styles.scss"
const IndexPage = () => {
  const backgroundStyle = {
    backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), 
url('../images/)`,

    backgroundSize: "cover",
    height: "100vh",
  }
  return (
    <div style={backgroundStyle}>
      <Layout>
        <Banner />
      </Layout>
    </div>
  )
}

export default IndexPage
